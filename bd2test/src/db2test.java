import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.sql.*;
import java.util.Arrays;
import java.util.List;

public class db2test {
	
	private static final String DATABASE = "ORI90DAT";

	public static void main(String[] args) {
				
		try {
			//System.out.println("Trying to connect...");
			String DRIVER = "com.ibm.as400.access.AS400JDBCDriver";
			String URL = "jdbc:as400://192.168.100.213";
			
			Class.forName(DRIVER);
			Connection conn = DriverManager.getConnection(URL, "qpgmr", "admincam");
			Statement stmt = conn.createStatement();
			
			FileWriter csvWriter = new FileWriter(System.getProperty("user.home") + "\\Documents\\AlveareConfig\\CalendarT2.csv");
        	
			String row;
			BufferedReader csvReader = new BufferedReader(new FileReader(System.getProperty("user.home") + "\\Documents\\AlveareConfig\\CalendarT.csv"));  
			int x = 0;
			int riga = 1;
			int newRiga = 1;
			String oldMatr = null;
			while ((row = csvReader.readLine()) != null) {
				List<String> data = Arrays.asList(row.split(";"));

				String errors = " ";
				String utente = data.get(0).substring(1) + "I";
				String MATRNA = data.get(2);
				String matrnaERROR = " ";
				String DSARNA = " ";
				String TCONNA = " ";
				int NCONNA = -1;
				String CDPAOO = " ";
				String DESC = " ";
				String CDARMA = " ";
				String DSARMA = " ";
				String DSCOCP = " ";
				String CDCFNC = " ";
				String DIP = " ";
				String CSUST = " ";
				String DTOROO = data.get(3);
				float PRZLML = 0;
				try{	
					
					if (!data.get(12).replaceAll(" ", "").equals("")) {
						//Query: TDOCOOA, NROROOA -> MATRNA
						ResultSet m = stmt.executeQuery("SELECT NRMTOOA, DTOROO, CDPAOO FROM " + DATABASE + ".OCMOVA0F, " + DATABASE + ".OCMOV00F WHERE TDOCOOA = '" + data.get(11) + "' AND NROROOA = " + data.get(12));
						if (m.next()) {
							MATRNA = m.getString("NRMTOOA");
							DTOROO = m.getString("DTOROO");
							CDPAOO = m.getString("CDPAOO");
							utente = data.get(0).substring(1) + "U";
						} else {
							//System.out.println("--- Codice matricola non valido ---");
							errors += "Tipo intervento e/o numero matricola non validi. ";
							matrnaERROR = "000000";
							MATRNA = matrnaERROR;
						}											
					}
					
					
					//Query: MATRNA -> DSARNA
					ResultSet q = stmt.executeQuery("SELECT DSARNA FROM " + DATABASE + ".AVAMA00F WHERE MATRNA = \'" + MATRNA + "\'");
					if (q.next()) {
						DSARNA = q.getString("DSARNA");
						//TCONNA = q.getString("TCONNA");
						//NCONNA = q.getInt("NCONNA");
						
						if(DSARNA.equals("")){
							MATRNA = matrnaERROR;
						}
					} else {
						//System.out.println("--- Codice matricola non valido ---");
						errors += "Codice matricola non valido. ";
						MATRNA = matrnaERROR;
					}
					
					//Query: MATRNA -> TCONNA, NCONNA
					ResultSet tn = stmt.executeQuery("SELECT TCONNC1, NCONNC1 FROM " + DATABASE + ".AVACO01F WHERE MATRNC1 = \'" + MATRNA + "\'");
					if (tn.next()) {
						
						TCONNA = tn.getString("TCONNC1");
						NCONNA = tn.getInt("NCONNC1");
					}
					
					if (!data.get(9).equals("")) {
						//Query: K_COD -> DESC, CDARMA, DSARMA
						ResultSet c = stmt.executeQuery("SELECT K_COD, \"DESC\", CDARMA, DSARMA FROM " + DATABASE + ".CSTABAI00L, " + DATABASE + ".MGART00F WHERE CDAR = CDARMA AND K_COD = '" + data.get(9) + "'");
						if (c.next()) {
							DESC = c.getString("DESC");
							CDARMA = c.getString("CDARMA");
							DSARMA = c.getString("DSARMA");
						} else {
							//System.out.println("--- Codice causale non valido ---");
							errors += "Codice causale non valido. ";
						}
					} else {
						//System.out.println("--- Codice causale non presente ---");
						errors += "Codice causale non presente. ";
					}
					
					//Query: TCONNA, NCONNA -> CDCFNC, DSCOCP
					ResultSet d = stmt.executeQuery("SELECT CDCFNC ,DSCOCP FROM " + DATABASE + ".AVACO00F, " + DATABASE + ".CGPCO00F WHERE TCONNC = '"+TCONNA+"' AND NCONNC = '"+NCONNA+"' AND CDCFNC = CONTCP");
					if (d.next()) {
						DSCOCP = d.getString("DSCOCP");
						CDCFNC = d.getString("CDCFNC");
					} else {
						//System.out.println("--- Codice matricola non valido ---");
						MATRNA = matrnaERROR;
					}
					
					//Query: K_COD (dipendente) -> CSUST, DESC
					ResultSet dp = stmt.executeQuery("SELECT K_COD, CSUST, \"DESC\" FROM " + DATABASE + ".CSTABPR00L WHERE K_COD = '" + data.get(0).substring(1, data.get(0).length()) + "'");
					if (dp.next()) {
						DIP = dp.getString("DESC");
						CSUST = dp.getString("CSUST");
					} else {
						//System.out.println("--- Codice dipendente non valido ---");
						errors += "Codice dipendente non valido. ";
					}
					
					//Query: CDCFNC, CDARMA -> PRZLML
					ResultSet l = stmt.executeQuery("SELECT DTUAML, DTLVML, PRZLML FROM " + DATABASE + ".MGLIS01F WHERE CLISML = '" + CDCFNC + "' AND CDARML = '" + CDARMA + "'");
					while (l.next() && PRZLML <= 0) {						
						
						int dateSS = l.getInt("DTUAML");
						int dateEE = l.getInt("DTLVML");
						
						if (Integer.parseInt(data.get(3)) >= dateSS && (Integer.parseInt(data.get(3)) <= dateEE || dateEE == 0)) {
							PRZLML = l.getFloat("PRZLML");
						}
						
					}
					
					String FATT = "S";
					if (MATRNA.substring(0, 1).equals("I")){
						FATT = "N";
					}
					
					//Start writing .csv after all queries
					if (oldMatr == null || !oldMatr.equals(data.get(2))) {
						riga = 1;
						newRiga = 1;
						if (!data.get(12).replaceAll(" ", "").equals("")) {
							//Query: TDOCOOA, NROROOA -> MAX(NRRGOO)
							ResultSet n = stmt.executeQuery("SELECT MAX(NRRGOO) AS NRRGOO FROM " + DATABASE + ".OCMOV01F WHERE TDOCOO = '" + data.get(11) + "' AND NROROO = " + data.get(12));
							if (n.next()) {
								newRiga = 1 + Integer.parseInt(n.getString("NRRGOO")) / 5; // riga = 1 + Integer.parseInt(n.getString("NRRGOO")) / 5;
							}
						} else {
							x++;
						}
						
						csvWriter.append(
								errors + ";" +
								MATRNA + ";" +
								DSARNA + ";" +
								CDARMA + ";" +
								DSARMA + ";" +
								" " + ";" +
								" " + ";" +
								DTOROO + ";" +
								" " + ";" +
								" " + ";" +
								" " + ";" +
								" " + ";" +
								" " + ";" +
								" " + ";" +
								" " + ";" +
								" " + ";" +
								data.get(11) + ";" +
								(data.get(12).replaceAll(" ", "").equals("") == true ? Integer.toString(x) : data.get(12)) + ";" +
								TCONNA + ";" +
								Integer.toString(NCONNA) + ";" +								
								utente + ";" +
								DIP + ";" +
								data.get(1) + ";" +								
								DTOROO + ";" +
								" " + ";" +
								" " + ";" +								
								CDCFNC + ";" +
								DSCOCP + ";" +
								CDPAOO + "\n"							
						);
						
					}
					oldMatr = data.get(2);
					
					csvWriter.append(
							errors + ";" +
							MATRNA + ";" +
							DSARNA + ";" +
							CDARMA + ";" +
							DSARMA + ";" +
							Integer.toString(newRiga * 5) + ";" +
							" " + ";" +
							data.get(3) + ";" +
							data.get(9) + ";" +
							DESC + ";" +
							" " + ";" +
							data.get(7) + ";" +
							(FATT.equals("S") == true ? data.get(8) : "0") + ";" +
							CSUST + ";" +
							Float.toString(PRZLML) + ";" +
							FATT + ";" +
							data.get(11) + ";" +
							(data.get(12).replaceAll(" ", "").equals("") == true ? Integer.toString(x) : data.get(12)) + ";" +
							TCONNA + ";" +
							Integer.toString(NCONNA) + ";" +
							utente + ";" +
							DIP + ";" +
							data.get(1) + ";" +							
							data.get(4) + ";" +
							data.get(5) + ";" +
							data.get(6) + ";" +
							CDCFNC + ";" +
							DSCOCP + ";" +
							CDPAOO + "\n"							
					);
					riga++;
					
					
					if (data.get(10).length() <= 40) {
						csvWriter.append(
								errors + ";" +
								MATRNA + ";" +
								DSARNA + ";" +
								CDARMA + ";" +
								DSARMA + ";" +
								Integer.toString(newRiga * 5) + ";" +
								"1" + ";" +
								data.get(3) + ";" +
								"ZZZ" + ";" +
								" " + ";" +
								data.get(10) + ";" +
								" " + ";" +
								" " + ";" +
								" " + ";" +
								" " + ";" +
								FATT + ";" +
								data.get(11) + ";" +
								(data.get(12).replaceAll(" ", "").equals("") == true ? Integer.toString(x) : data.get(12)) + ";" +
								TCONNA + ";" +
								Integer.toString(NCONNA) + ";" +
								utente + ";" +
								DIP + ";" +
								data.get(1) + ";" +
								data.get(4) + ";" +
								" " + ";" +
								" " + ";" +
								CDCFNC + ";" +
								DSCOCP + ";" +
								CDPAOO + "\n"							
						);
						riga++;
					} else {
						boolean ended = false;
						int start = 40;
						int stop = data.get(10).length();
						int tmp = 0;
						int sottoR = 1;
						
						while (!ended) {
							if (start == stop) {
								ended = true;
							} else {
								int old = start;
								while (data.get(10).charAt(start) != ' ') {
									start--;
								}
								if (start <= tmp) {
									start = old;
								}
							}
							
							csvWriter.append(
									errors + ";" +
									MATRNA + ";" +
									DSARNA + ";" +
									CDARMA + ";" +
									DSARMA + ";" +
									Integer.toString(newRiga * 5) + ";" +
									Integer.toString(sottoR) + ";" +
									data.get(3) + ";" +
									"ZZZ" + ";" +
									" " + ";" +
									data.get(10).substring(tmp, start) + ";" +
									" " + ";" +
									" " + ";" +
									" " + ";" +
									" " + ";" +
									FATT + ";" +
									data.get(11) + ";" +
									(data.get(12).replaceAll(" ", "").equals("") == true ? Integer.toString(x) : data.get(12)) + ";" +
									TCONNA + ";" +
									Integer.toString(NCONNA) + ";" +
									utente + ";" +
									DIP + ";" +
									data.get(1) + ";" +
									data.get(4) + ";" +
									" " + ";" +
									" " + ";" +
									CDCFNC + ";" +
									DSCOCP + ";" +
									CDPAOO + "\n"									
							);
												
							tmp = start + 1;
							start += 40;
							riga++;
							sottoR++;
							
							if (start >= stop) {
								start = stop;
							}
						}						
					}		
					newRiga++;
					
					
				}catch(Exception e){
					System.out.println(e);
				}				
				
			}
			csvReader.close();
			csvWriter.flush();  
            csvWriter.close();
			conn.close();
			
			orderCSV();
			
			File f = new File(System.getProperty("user.home") + "\\Documents\\AlveareConfig\\CalendarT.csv");
			f.delete();
            
			
			Runtime.getRuntime().exec("cmd /c start excel.exe " + System.getProperty("user.home") + "\\Documents\\AlveareConfig\\Calendar.csv");
			
			System.exit(0);
			
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	private static void orderCSV() {
		try {
			BufferedReader csvReader = new BufferedReader(new FileReader(System.getProperty("user.home") + "\\Documents\\AlveareConfig\\CalendarT2.csv"));
			FileWriter csvWriter = new FileWriter(System.getProperty("user.home") + "\\Documents\\AlveareConfig\\Calendar.csv");
			csvWriter.append("ERRORS;MATRNA;DSARNA;CDARMA;DSARMA;N RIGA;SOTTO-RIGA;DATESTART;K_COD;DESC;DETAIL;ORE;FATTURATE;CSUST;PRZLML;FATTURA S/N;TIPO INTERVENTO; NUMERO INTERVENTO; TCONNA; NCONNA;CDRINI;DIP;SUMMARY;DATEEND;TIMESTART;TIMEEND;CDCFNC;DSCOCP;CDPAOO\n");
        	
			
        	String row = null;
        	while ((row = csvReader.readLine()) != null) {
        		
				List<String> data = Arrays.asList(row.split(";"));
        		if (!data.get(1).replaceAll(" ", "").equals("")) {
        			csvWriter.append(row + "\n");
        		}
        	}

            csvReader.close();
            
            csvReader = new BufferedReader(new FileReader(System.getProperty("user.home") + "\\Documents\\AlveareConfig\\CalendarT2.csv"));
            while ((row = csvReader.readLine()) != null) {
        		
				List<String> data = Arrays.asList(row.split(";"));
        		if (data.get(1).replaceAll(" ", "").equals("")) {
        			csvWriter.append(row + ";\n");
        		}
        	}
            csvReader.close();
			
            
            csvWriter.flush();  
            csvWriter.close();
            
            File f = new File(System.getProperty("user.home") + "\\Documents\\AlveareConfig\\CalendarT2.csv");
			f.delete();
			
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}	
}