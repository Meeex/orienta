

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.Arrays;
import java.util.List;

public class RecursiveImmission {
	
	private static final int TESTATA = 0;
	private static final int RIGA1 = 1;
	private static final int RIGA2N = 2;
	
	private static final String DATABASE = "ORI90DAT";

	public static void main(String[] args) {
		//Reading user's configurations
        String dateS = null;
        String dateE = null;
        String ursConfigPath = null;
        String tipoIntervento = null;        
        String matricola = null;
        
        //Path to config's file          
        try (FileReader reader = new FileReader(System.getProperty("user.home") + "\\Documents\\AlveareConfig\\config.ini");
            BufferedReader br = new BufferedReader(reader)) {

        	ursConfigPath = br.readLine();
        	dateS = br.readLine();
        	dateE = br.readLine();  	
        	tipoIntervento = br.readLine();

        	
        	FileReader readerM = new FileReader(ursConfigPath + "\\config.ini");
            BufferedReader brM = new BufferedReader(readerM);
            matricola = brM.readLine();            
        	
        } catch (IOException e) {
        	System.err.format("IOException: %s%n", e);
        }
		
		try {
			//Connecting to DB2
			//System.out.println("Trying to connect...");
			String DRIVER = "com.ibm.as400.access.AS400JDBCDriver";
			String URL = "jdbc:as400://192.168.100.213";
			
			Class.forName(DRIVER);
			Connection conn = DriverManager.getConnection(URL, "qpgmr", "admincam");
			Statement stmt = conn.createStatement();
			
			
			String row;
			BufferedReader csvReader = new BufferedReader(new FileReader(System.getProperty("user.home") + "\\Documents\\AlveareConfig\\Calendar.csv"));  
			
			int state = TESTATA;
			
			
			csvReader.readLine();
			
			int oldN = 0;
			
			while ((row = csvReader.readLine()) != null && !Arrays.asList(row.split(";")).get(1).replaceAll(" ", "").equals("")) {
				List<String> data = Arrays.asList(row.split(";"));
				
				String MATRNA = data.get(1);
				String DSARNA = data.get(2);
				String CDARMA = data.get(3);
				String DSARMA = data.get(4);
				String N_RIGA = data.get(5);
				String SOTTO_RIGA = data.get(6);
				String DATESTART = data.get(7);
				String K_COD = data.get(8);
				String DESC = data.get(9);
				String DETAIL = data.get(10).toUpperCase();
				String ORE = data.get(11);
				String FATTURATE = data.get(12);				
				String CSUST = data.get(13);
				String PRZLML = data.get(14);
				String FATTURA_SN = data.get(15);
				String TIPO_INTERVENTO = data.get(16);
				String NUMERO_INTERVENTO = data.get(17);				
				String TCONNA = data.get(18);
				String NCONNA = data.get(19);
				String CDRINI = data.get(20);
				String DIP = data.get(21);
				String SUMMARY = data.get(22);
				String DATEEND = data.get(23);
				String TIMESTART = data.get(24);
				String TIMEEND = data.get(25);
				String CDCFNC = data.get(26);
				String DSCOCP = data.get(27);
				String CDPAOO = data.get(28);
				if (FATTURA_SN.equals("S") || FATTURA_SN.replaceAll(" ", "").equals("") || FATTURA_SN.replaceAll(" ", "").equals("S")) {
					FATTURA_SN = " ";
				} else if (FATTURA_SN.equals("N") || FATTURA_SN.replaceAll(" ", "").equals("N")) {
					FATTURA_SN = "S";
				}
				

				
				
				if (!MATRNA.replaceAll(" ", "").equals("")) {
					
					if(!K_COD.equals("") &&!K_COD.equals(" ") && !K_COD.substring(0, 2).equals("ZZ")){
						state = RIGA1;
					}
					if (state == RIGA2N && oldN != Integer.parseInt(NUMERO_INTERVENTO)) {
						state = TESTATA;
					}
					oldN = Integer.parseInt(NUMERO_INTERVENTO);
					
					String query = null;
					switch (state) {
						case TESTATA:
							
							try{
								query = "INSERT INTO " + DATABASE + ".OCMOV00W (CDDTOO,TDOCOO,NROROO,CDCFOO,DTOROO,PROFOO, CDPAOO) VALUES ('01', " + tipoIntervento + ", " + NUMERO_INTERVENTO + ", " + CDCFNC + ", " + DATESTART + ", 'UT" + CDRINI + "', '" + CDPAOO + "')";
								//System.out.println(query);
								stmt.executeUpdate(query);
								
								query = "INSERT INTO " + DATABASE + ".OCMOVA0W (CDDTOOA,TDOCOOA,NROROOA,FLFZOOA,NRMTOOA,TCONOOA,NCONOOA,FUCCOOA,PROFOOA,CTEAOOA,TIPCOOA,TPFIOOA) VALUES ('01', " + tipoIntervento + ", " + NUMERO_INTERVENTO + ", ' ', '" + MATRNA + "', '" + TCONNA + "', " + NCONNA + ", 'N', 'UT" + CDRINI + "', '" + matricola + "', '02', '05')";
								//System.out.println(query);
								stmt.executeUpdate(query);
								
							}catch(Exception e){
								System.out.println(e);
							}
							
							state = RIGA1;
							break;
						case RIGA1:
							
							try{
								query = "INSERT INTO " + DATABASE + ".OCMOV01W (CDDTOO,TDOCOO,NROROO,NRRGOO,DTRCOO,QTMOOO,QTFAOO,SACNOO,PROFOO,QTOROO,DSAROO,PRZUOO,COMSOO,TIMOOO,CDAROO) VALUES ('01', " + tipoIntervento + ", " + NUMERO_INTERVENTO + ", " + N_RIGA + ", " + DATESTART + ", " + ORE + ", " + FATTURATE + ", 'S', 'UT" + CDRINI + "', " + ORE + ", '" + DSARMA + "', " + PRZLML + ", " + CSUST + ", 57, '" + CDARMA + "')";
								//System.out.println(query);
								stmt.executeUpdate(query);
								
								query = "INSERT INTO " + DATABASE + ".OCMOV03W (CDDTO3,TDOCO3,NRORO3,NRRGO3,CAIMO3,MATRO3,CTEAO3,TCONO3,NCONO3,PROFO3,DTASO3,NFATO3,DTRIO3) VALUES ('01', " + tipoIntervento + ", " + NUMERO_INTERVENTO + ", " + N_RIGA + ", '"+ K_COD +"', '"+ MATRNA +"', '"+matricola+"', '"+TCONNA+"', " + NCONNA + ", 'UT" + CDRINI + "', "+DATESTART+", '"+ FATTURA_SN +"', " + DATESTART + ")";
								stmt.executeUpdate(query);
								
								/*
								if (K_COD.replaceAll(" ", "").equals("ZZZ")) {
									
									query = "INSERT INTO " + DATABASE + ".OCCOM00W (PROFOC,CDDTOC,TDOCOC,NROROC,NRRGOC,RGCOOC,TIMOOC,TCOMOC) VALUES ('UT" + CDRINI + "', '01', " + tipoIntervento + ", " + NUMERO_INTERVENTO + ", " + N_RIGA + ", " + RIGA_COMMENTO + ", 96, '" + DETAIL + "')";
									stmt.executeUpdate(query);		
									RIGA_COMMENTO++;
								}
								*/
								
							}catch(Exception e){
								System.out.println(e);
							}
							
							state = RIGA2N;						
							break;
						case RIGA2N:
							
							try{
									
								query = "INSERT INTO " + DATABASE + ".OCCOM00W (PROFOC,CDDTOC,TDOCOC,NROROC,NRRGOC,RGCOOC,TIMOOC,TCOMOC) VALUES ('UT" + CDRINI + "', '01', " + tipoIntervento + ", " + NUMERO_INTERVENTO + ", " + N_RIGA + ", " + SOTTO_RIGA + ", 96, '" + DETAIL + "')";
								stmt.executeUpdate(query);		
								
							}catch(Exception e){
								System.out.println(e);
							}
							
							break;
					}
				}
			}
			csvReader.close();
			conn.close();
            
			System.exit(0);
			
		} catch (Exception e) {
			System.out.println(e);
		}

	}

}
