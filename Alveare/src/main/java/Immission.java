package main.java;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.Arrays;
import java.util.List;

public class Immission {

	public static boolean run() {
		//Reading user's configurations
        String dateS = null;
        String dateE = null;
        String ursConfigPath = null;
        String tipoIntervento = null;        
        String matricola = null;
        
        //Path to config's file          
        try (FileReader reader = new FileReader(System.getProperty("user.home") + "\\Documents\\AlveareConfig\\config.ini");
            BufferedReader br = new BufferedReader(reader)) {

        	ursConfigPath = br.readLine();
        	dateS = br.readLine();
        	dateE = br.readLine();  	
        	tipoIntervento = br.readLine();

        	
        	FileReader readerM = new FileReader(ursConfigPath + "\\config.ini");
            BufferedReader brM = new BufferedReader(readerM);
            matricola = brM.readLine();            
        	
        } catch (IOException e) {
        	System.err.format("IOException: %s%n", e);
        }
		
        return checkPendingErrors();		
	}

	private static boolean checkPendingErrors() {
		String row;
		BufferedReader csvReader;
		try {
			csvReader = new BufferedReader(new FileReader(System.getProperty("user.home") + "\\Documents\\AlveareConfig\\Calendar.csv"));		  
		
			csvReader.readLine();			
						
			while ((row = csvReader.readLine()) != null) {
				List<String> data = Arrays.asList(row.split(";"));
				
				String ERRORS = data.get(0);
				String MATRNA = data.get(1);	
				
				
				if (!MATRNA.replaceAll(" ", "").equals("")) {
					if (!ERRORS.replaceAll(" ", "").equals("")){
						csvReader.close();
						return false;
					}
				}
			}	
			
			csvReader.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}

}
