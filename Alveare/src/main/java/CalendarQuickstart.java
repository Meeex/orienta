import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.DateTime;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.CalendarScopes;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.Events;

import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.GeneralSecurityException;
import java.sql.*;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.YearMonth;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.UIManager;

import main.java.Immission;
import main.java.DatePicker;;


public class CalendarQuickstart {
	
	private static boolean execute = false;
	private static boolean update = false;
    private static final String APPLICATION_NAME = "JProfit Google Calendar API";
    private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();

    /**
     * Global instance of the scopes required.
     * If modifying these scopes, delete your previously saved tokens/ folder.
     */
    private static final List<String> SCOPES = Collections.singletonList(CalendarScopes.CALENDAR_READONLY);
    
    //
    private static final String CREDENTIALS_FILE_PATH = "/credentials.json";
    //
    
    /**
     * Creates an authorized Credential object.
     * @param HTTP_TRANSPORT The network HTTP Transport.
     * @param ursConfigPath 
     * @return An authorized Credential object.
     * @throws IOException If the credentials.json file cannot be found.
     */
    private static Credential getCredentials(final NetHttpTransport HTTP_TRANSPORT, String ursConfigPath) throws IOException {
        // Load client secrets.
        InputStream in = CalendarQuickstart.class.getResourceAsStream(CREDENTIALS_FILE_PATH);
        if (in == null) {
            throw new FileNotFoundException("Resource not found: " + CREDENTIALS_FILE_PATH);
        }
        GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));

        // Build flow and trigger user authorization request.
        GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
                HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPES)
                .setDataStoreFactory(new FileDataStoreFactory(new java.io.File(ursConfigPath + "\\tokens")))
                .setAccessType("offline")
                .build();
        LocalServerReceiver receiver = new LocalServerReceiver.Builder().setPort(8888).build();
        return new AuthorizationCodeInstalledApp(flow, receiver).authorize("user");
    }

    public static void main(String... args) throws IOException, GeneralSecurityException {
    	
    	GUI();
    	
    	/*
    	while (!execute) {
    		try {
    			Thread.sleep(1000);
    		} catch (Exception e) {
    			System.out.println(e);
    		}
    	}
    	*/
    	
    	
    }

	private static String toStringExtended(int x) {
		String numberX = "";
		String num = Integer.toString(x);
		
		if (num.length() == 1){
			numberX = "000" + num;
		} else if (num.length() == 2){
			numberX = "00" + num;
		} else if (num.length() == 3){
			numberX = "0" + num;
		} else if (num.length() == 1){
			numberX = num;
		}
		
		return numberX;
	}

	private static void GUI() {		
		
		JFrame frame = new JFrame("JProfit");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //Set up the content pane.
        addComponentsToPane(frame);

        //Display the window.
        frame.pack();
        frame.setVisible(true);
		
	}
	
	 private static void addComponentsToPane(JFrame frame) {
		 
		Container pane = frame.getContentPane();
		//Reading user's configurations
        String dateS = null;
        String dateE = null;
        String ursConfigPath = null;
        String tipoIntervento = null;        
        String matricola = null;
        
        //Path to config's file          
        try (FileReader reader = new FileReader(System.getProperty("user.home") + "\\Documents\\AlveareConfig\\config.ini");
            BufferedReader br = new BufferedReader(reader)) {

        	ursConfigPath = br.readLine();
        	dateS = br.readLine();
        	dateE = br.readLine();  	
        	tipoIntervento = br.readLine();

        	
        	FileReader readerM = new FileReader(ursConfigPath + "\\config.ini");
            BufferedReader brM = new BufferedReader(readerM);
            matricola = brM.readLine();            
        	
        } catch (IOException e) {
        	System.err.format("IOException: %s%n", e);
        }

	    JButton button;
	    JTextField textField;
	    JLabel label;
		pane.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		
		//natural height, maximum width
		c.fill = GridBagConstraints.HORIZONTAL;
		
		//title
		label = new JLabel("Configurazione JProfit 4.1");
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 1;
		c.gridy = 0;
		c.insets = new Insets(25,135,10,0);
		label.setFont(label.getFont().deriveFont(22.0f));
		pane.add(label, c);
		
		
		//date start
		label = new JLabel("Data Inizio (YYYY-MM-DD): ");
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 0;
		c.gridy = 2;
		c.insets = new Insets(5,30,5,5);
		pane.add(label, c);
		
		JTextField textFieldDateS = new JTextField(20);
		textFieldDateS.setText(dateS);
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 1;
		c.gridy = 2;
		pane.add(textFieldDateS, c);
		
		final JButton buttonDateS = new JButton("Seleziona");
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 2;
		c.gridy = 2;
		
		c.ipady = -7;
		//c.insets = new Insets(25,30,5,20);
		
		buttonDateS.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				textFieldDateS.setText(new DatePicker(frame).setPickedDate());
			}
		});
		
		pane.add(buttonDateS, c);
		c.ipady = 0;	
		
		//date end
		label = new JLabel("Data Fine (YYYY-MM-DD): ");
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 0;
		c.gridy = 3;
		c.insets = new Insets(5,30,5,5);
		pane.add(label, c);
					
		JTextField textFieldDateE = new JTextField(20);
		textFieldDateE.setText(dateE);
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 1;
		c.gridy = 3;
		pane.add(textFieldDateE, c);
		
			
		final JButton buttonDate = new JButton("Seleziona");
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 2;
		c.gridy = 3;
		
		c.ipady = -7;
		//c.insets = new Insets(25,30,5,20);
		
		buttonDate.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				textFieldDateE.setText(new DatePicker(frame).setPickedDate());
			}
		});
		
		pane.add(buttonDate, c);
		c.ipady = 0;		
		
		
		//identification number
		label = new JLabel("Tecnico (XXXX): ");
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 0;
		c.gridy = 4;
		c.insets = new Insets(5,30,5,5);
		pane.add(label, c);
		
		JTextField textFieldMatr = new JTextField(4);
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 1;
		c.gridy = 4;
		textFieldMatr.setText(matricola);
		pane.add(textFieldMatr, c);
		
		//path
		label = new JLabel("Path cartella personale: ");
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 0;
		c.gridy = 1;
		c.insets = new Insets(25,30,5,5);
		pane.add(label, c);
		
		final JTextField textFieldC = new JTextField(40);
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 1;
		c.gridy = 1;
		textFieldC.setText(ursConfigPath);
		pane.add(textFieldC, c);
		
		final JButton buttonC = new JButton("Apri");
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 2;
		c.gridy = 1;
		c.ipady = -7;
		c.insets = new Insets(25,30,5,20);
		
		buttonC.addActionListener(new ActionListener() {

		    @Override
		    public void actionPerformed(ActionEvent e) {
		    	JFileChooser chooser = new JFileChooser(); 
		    	chooser.updateUI();		    	
		        chooser.setCurrentDirectory(new java.io.File("."));
		        chooser.setDialogTitle("Seleziona la cartella");
		        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		        //
		        // disable the "All files" option.
		        //
		        chooser.setAcceptAllFileFilterUsed(false);
		        //    
		        chooser.showOpenDialog(buttonC);
		        if (chooser.getSelectedFile() != null) {
		        	textFieldC.setText(chooser.getSelectedFile().getAbsolutePath());
		        	
		        	try {		                	
	                	FileReader readerM = new FileReader(textFieldC.getText() + "\\config.ini");
	                    BufferedReader brM = new BufferedReader(readerM);
	                    textFieldMatr.setText(brM.readLine());          
	                	
	                } catch (Exception e1) {
	                	System.err.format("IOException: %s%n", e1);
	                }
		        }
		    }
		});
		
		pane.add(buttonC, c);
		c.ipady = 0;
						
		//type
		label = new JLabel("Tipo intervento: ");
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 0;
		c.gridy = 5;
		c.insets = new Insets(5,30,5,5);
		pane.add(label, c);
		
		JTextField textFieldInt = new JTextField(4);
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 1;
		c.gridy = 5;
		textFieldInt.setText(tipoIntervento);
		pane.add(textFieldInt, c);

	
		//annulla
		JButton buttonA = new JButton("Annulla");
		c.fill = GridBagConstraints.HORIZONTAL;
		c.ipady = 0;       //reset to default
		c.weighty = 1.0;   //request any extra vertical space
		c.anchor = GridBagConstraints.PAGE_END; //bottom of space
		c.insets = new Insets(10,30,10,20);  //top padding
		c.gridx = 2;       //aligned with button 2
		c.gridwidth = 1;   //2 columns wide
		c.gridy = 6;       //third row
		
		buttonA.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
			
		});
		
		pane.add(buttonA, c);
		
		//salva
		button = new JButton("Salva");
		c.fill = GridBagConstraints.HORIZONTAL;
		c.ipady = 0;       //reset to default
		c.weighty = 1.0;   //request any extra vertical space
		c.anchor = GridBagConstraints.PAGE_END; //bottom of space
		c.insets = new Insets(10,0,10,20);
		c.gridx = 3;       //aligned with button 2
		c.gridwidth = 1;   //2 columns wide
		c.gridy = 6;       //third row
		
		button.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				saveConfig(textFieldDateS, textFieldDateE, textFieldMatr, textFieldC, textFieldInt);
			}
			
		});
		
		pane.add(button, c);	
		
		//result label
		JLabel resultLabel = new JLabel("");
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 2;
		c.gridy = 7;
		c.insets = new Insets(20,30,35,0);
		resultLabel.setVisible(false);
		pane.add(resultLabel, c);
		
		//fine
		JButton buttonF = new JButton("- FIN -");
		c.fill = GridBagConstraints.HORIZONTAL;
		c.ipady = 0;       //reset to default
		c.weighty = 1.0;   //request any extra vertical space
		c.anchor = GridBagConstraints.PAGE_END; //bottom of space
		c.insets = new Insets(10,0,30,30);  //top padding
		c.gridx = 4;       //aligned with button 2
		c.gridwidth = 1;   //2 columns wide
		c.gridy = 7;       //third row
		buttonF.setVisible(false);
		
		buttonF.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
			
		});
		
		pane.add(buttonF, c);
		
		//update db2
		JButton buttonU = new JButton("Update Database");
		buttonU.setEnabled(false);
		c.fill = GridBagConstraints.HORIZONTAL;
		c.ipady = 0;       //reset to default
		c.weighty = 1.0;   //request any extra vertical space
		c.anchor = GridBagConstraints.PAGE_END; //bottom of space
		c.insets = new Insets(20,30,30,5);
		c.gridx = 1;       //aligned with button 2
		c.gridwidth = 1;   //2 columns wide
		c.gridy = 7;       //third row
		
		buttonU.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					
					if (Immission.run()) {
						
						update = true;
						
						buttonU.setEnabled(false);
						
						Process proc = Runtime.getRuntime().exec("java -jar updateDB.jar");
						InputStream in = proc.getInputStream();
					    InputStream err = proc.getErrorStream();
					    
					    
					    buttonF.setVisible(true);
						resultLabel.setText("Fatto!");
						resultLabel.setFont(resultLabel.getFont().deriveFont(14.0f));
						resultLabel.setVisible(true);
					} else {
						resultLabel.setFont(resultLabel.getFont().deriveFont(12.0f));
						resultLabel.setText("Errori in sospeso.");
						resultLabel.setVisible(true);
					}
					
				} catch (Exception e) {
					System.out.println(e);
				}			    
			}
			
		});
		
		pane.add(buttonU, c);
		
		//open exel
		JButton buttonX = new JButton("Apri Exel");
		c.fill = GridBagConstraints.HORIZONTAL;
		c.ipady = 0;       //reset to default
		c.weighty = 1.0;   //request any extra vertical space
		c.anchor = GridBagConstraints.PAGE_END; //bottom of space
		c.insets = new Insets(20,30,30,10);  //top padding
		c.gridx = 0;       //aligned with button 2
		c.gridwidth = 1;   //2 columns wide
		c.gridy = 7;       //third row
		
		buttonX.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {

				try {
					Runtime.getRuntime().exec("cmd /c start excel.exe " + System.getProperty("user.home") + "\\Documents\\AlveareConfig\\Calendar.csv");
				} catch (Exception e){
					System.out.println(e);
				}
				buttonU.setEnabled(true);
			}
			
		});
		
		pane.add(buttonX, c);
		
		//esegui
		JButton buttonE = new JButton("Scarica Calendar");
		c.fill = GridBagConstraints.HORIZONTAL;
		c.ipady = 0;       //reset to default
		c.weighty = 1.0;   //request any extra vertical space
		c.anchor = GridBagConstraints.PAGE_END; //bottom of space
		c.insets = new Insets(10,0,10,30);
		c.gridx = 4;       //aligned with button 2
		c.gridwidth = 1;   //2 columns wide
		c.gridy = 6;       //third row
		
		buttonE.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				saveConfig(textFieldDateS, textFieldDateE, textFieldMatr, textFieldC, textFieldInt);
				buttonU.setEnabled(true);
				//buttonE.setEnabled(false);
				//execute = true;
				try{
					downloadCalendar();
				} catch (Exception e) {
					System.out.println(e);
				}
				
			}
			
		});

		pane.add(buttonE, c);
						
	}

	protected static void saveConfig(JTextField textFieldDateS, JTextField textFieldDateE, JTextField textFieldMatr, JTextField textFieldC, JTextField textFieldInt) {
		try {
			FileWriter configWriter = new FileWriter(System.getProperty("user.home") + "\\Documents\\AlveareConfig\\config.ini");
			
			configWriter.append(textFieldC.getText() + "\n");			
			configWriter.append(textFieldDateS.getText() + "\n");
			configWriter.append(textFieldDateE.getText() + "\n");
			configWriter.append(textFieldInt.getText() + "\n");
			
			configWriter.close();
			
			
			FileWriter configWriterP = new FileWriter(textFieldC.getText() + "\\config.ini");
			configWriterP.append(textFieldMatr.getText() + "\n");
			configWriterP.close();
			
		} catch (Exception e) {
			System.out.println(e);
		}
		
	}
	
	protected static void downloadCalendar() throws IOException, GeneralSecurityException {
		//Reading user's configurations
        String dateS = null;
        String dateE = null;
        String ursConfigPath = null;
        String tipoIntervento = null;        
        String matricola = null;
        
        //Path to config's file          
        try (FileReader reader = new FileReader(System.getProperty("user.home") + "\\Documents\\AlveareConfig\\config.ini");
            BufferedReader br = new BufferedReader(reader)) {

        	ursConfigPath = br.readLine();
        	dateS = br.readLine();
        	dateE = br.readLine();  	
        	tipoIntervento = br.readLine();

        	
        	FileReader readerM = new FileReader(ursConfigPath + "\\config.ini");
            BufferedReader brM = new BufferedReader(readerM);
            matricola = brM.readLine();            
        	
        } catch (IOException e) {
        	System.err.format("IOException: %s%n", e);
        }
        
     // Build a new authorized API client service.
        final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
        Calendar service = new Calendar.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT, ursConfigPath))
                .setApplicationName(APPLICATION_NAME)
                .build();
        
        //Getting user's events from Google Calendar of the specified month
        //YearMonth yearMonthObject = YearMonth.of(Integer.parseInt(date.substring(0, 4)), Integer.parseInt(date.substring(5, 7)));        
        dateS = dateS.concat("T00:00:00+00:00");
        DateTime now = new DateTime(dateS);
        
        dateE = dateE.concat("T00:00:00+00:00");
        DateTime max = new DateTime(dateE);
        DateTime end1 = new DateTime((max.getValue()/1000 + (24 * 60 * 60)) * 1000);  
        
        String calendarID = "primary"; //default calendar
        File tempFile = new File(ursConfigPath + "\\tokens\\CalendarID.txt");
        if (tempFile.exists()) {
        	try (FileReader reader = new FileReader(ursConfigPath + "\\tokens\\CalendarID.txt");
                BufferedReader br = new BufferedReader(reader)) {

            	calendarID = br.readLine();            	
            	
            } catch (IOException e) {
            	System.err.format("IOException: %s%n", e);
            }
        }
        
        Events events = service.events().list(calendarID)  //Calendar ID
                .setMaxResults(100000)
                .setTimeMin(now)
                .setTimeMax(end1)
                .setOrderBy("startTime")
                .setSingleEvents(true)
                .execute();
        
        List<Event> items = events.getItems();        
        if (items.isEmpty()) {
            System.out.println("---Nessun evento trovato---");
        } else {
        	//Creating the .csv file 
        	FileWriter csvWriter = new FileWriter(System.getProperty("user.home") + "\\Documents\\AlveareConfig\\CalendarT.csv");
        	Map<String, String> sortedMap = new TreeMap<String, String>();
        	int x = 0;
        	
            for (Event event : items) {
            	DateTime start = event.getStart().getDateTime();
            	DateTime end = event.getEnd().getDateTime();            	
            	String tS = "000000";
            	String tE = "000000";
            	String codCausale = " ";
            	String numeroOrdine = " ";
            	int timeStart = 0;
            	int timeEnd = 0;
            	double ore = 0;

            	int startCC = 0;
            	int endCC = 0;
            	for (int i = 0; i < event.getSummary().trim().length(); i++) {
            		if (event.getSummary().trim().charAt(i) == '[') {
            			startCC = i;
            		} else if(event.getSummary().trim().charAt(i) == ']') {
            			endCC = i;
            		}
            	}
            	
            	if (startCC != 0 && endCC != 0 && startCC < endCC) {
            		codCausale = event.getSummary().trim().substring(startCC + 1, endCC).toUpperCase();
            	}
            	
            	
            	if (start == null) {
            		//Handling multi-days events
                    start = event.getStart().getDate();
                    end = event.getEnd().getDate();
                } else {
                	tS = start.toString().substring(11, 13) + start.toString().substring(14, 16) + start.toString().substring(17, 19);
                	tE = end.toString().substring(11, 13) + end.toString().substring(14, 16) + end.toString().substring(17, 19);
                	timeStart = (Integer.parseInt(start.toString().substring(11, 13)) * 60 + Integer.parseInt(start.toString().substring(14, 16))) * 60;
                	timeEnd = (Integer.parseInt(end.toString().substring(11, 13)) * 60 + Integer.parseInt(end.toString().substring(14, 16))) * 60;
                	ore = (int)((timeEnd - timeStart) / 36.0);
                	ore = ore / 100.0;
                }
            	
            	int startNO = -1;
            	int endNO = -1;
            	for (int i = 0; i < event.getSummary().trim().length(); i++) {
            		if (event.getSummary().trim().charAt(i) == '(') {
            			startNO = i;
            		} else if(event.getSummary().trim().charAt(i) == ')') {
            			endNO = i;
            		}
            	}
            	
            	if (startNO != -1 && endNO != -1 && startNO < endNO) {
            		numeroOrdine = event.getSummary().trim().substring(startNO + 1, endNO);
            	}
            	
            	//Row's value
            	String value = "M" + matricola + ";" +
                		event.getSummary().trim().replaceAll("\r\n|\r|\n", " ").replaceAll(";", ",") + ";" + 
                		event.getSummary().replace('(', ' ').replace(')', ' ').substring(0, (event.getSummary().length() < 6 ? event.getSummary().length() : 6)).toUpperCase() + ";" +                 		
                	    start.toString().substring(0, 4) + start.toString().substring(5, 7) + start.toString().substring(8, 10) + ";" + 
                	    end.toString().substring(0, 4) + end.toString().substring(5, 7) + end.toString().substring(8, 10) + ";" + 
                	    tS + ";" + 
                	    tE + ";" + 
                	    ore + ";" + 
                	    ore + ";" + 
                	    codCausale + ";" +
                	    (event.getDescription() == null ? "" : event.getDescription().trim().replaceAll("\r\n|\r|\n", " ").replaceAll(";", ",").replaceAll("'", "").replaceAll(":", "").replaceAll("-", "").replaceAll("/?", "")) + ";" +
                	    tipoIntervento + ";" +
                	    numeroOrdine + ";|\n";
            	
            	//Ordering's keys of row 
            	String key = event.getSummary().substring(0, (event.getSummary().length() < 6 ? event.getSummary().length() : 6)).toUpperCase() + toStringExtended(x);
            	
            	sortedMap.put(key, value);
            	x++;
            	
            }
            
            for (String val : sortedMap.values()) {
            	csvWriter.append(val);
            }
            
            csvWriter.flush();  
            csvWriter.close();
        }
        
        //Execute db.jar witch connects to the server
        Process proc = Runtime.getRuntime().exec("java -jar db.jar");
	    // Then retreive the process output
	    InputStream in = proc.getInputStream();
	    InputStream err = proc.getErrorStream();
	}
}